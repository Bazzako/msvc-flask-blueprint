from app.users import bp
from app.extensions import db
from app.models.users import Users, UsersIn, UsersOut, LoginIn, LoginOut
from app.auth import token_auth



####
## view functions
####    
@bp.get('/<int:users_id>')
@bp.auth_required(token_auth)
@bp.output(UsersOut)
def get_user(users_id):
    return db.get_or_404(Users, users_id)

@bp.get('/')
@bp.auth_required(token_auth)
@bp.output(UsersOut(many=True))
def get_users():
    return Users.query.all()

@bp.post('/')
@bp.auth_required(token_auth)
@bp.input(UsersIn, location='json')
@bp.output(UsersOut, status_code=201)
def create_users(json_data):
    users = Users(**json_data)
    db.session.add(users)
    db.session.commit()
    return users

@bp.patch('/<int:users_id>')
@bp.auth_required(token_auth)
@bp.input(UsersIn(partial=True), location='json')
@bp.output(UsersOut)
def update_users(users_id, json_data):
    users = db.get_or_404(Users, users_id)
    for attr, value in json_data.items():
        setattr(users, attr, value)
    db.session.commit()
    return users

@bp.delete('/<int:users_id>')
@bp.auth_required(token_auth)
@bp.output({}, status_code=204)
def delete_users(users_id):
    users = db.get_or_404(Users, users_id)
    db.session.delete(users)
    db.session.commit()
    return ''

@bp.post('/login')
@bp.input(LoginIn, location='json')
@bp.output(LoginOut, status_code=201)
def login_user(json_data):
    # Find user by email
    user = Users.query.filter_by(email=json_data.get('email')).first()
    # If user doesn't exist or password is wrong
    if not user or not user.check_password(json_data.get('password')):
        return {'message': 'Invalid email or password'}, 401
    
    token = user.generate_auth_token(600)
    return { 'token': token, 'duration': 600 }
