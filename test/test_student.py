from test import client
import pytest

def test_get_students(client):
    response0 = client.post("/users/login", json={'email': 'dennis@dennis1.com', 'password': 'dennis1234'})
    access_token = response0.json["token"]
    headers = { 'Authorization': 'Bearer {}'.format(access_token) }
    response = client.get("/students/", headers=headers)
    assert response.status_code == 200
    assert response.json[4]['name'] == 'Mega Tron'

def test_get_student(client):
    response0 = client.post("/users/login", json={'email': 'dennis@dennis1.com', 'password': 'dennis1234'})
    access_token = response0.json["token"]
    headers = { 'Authorization': 'Bearer {}'.format(access_token) }
    response = client.get("/students/2", headers=headers)
    assert response.status_code == 200
    assert response.json["name"] == "Sam Sung"

def test_post_student(client):
    response0 = client.post("/users/login", json={'email': 'dennis@dennis1.com', 'password': 'dennis1234'})
    access_token = response0.json["token"]
    headers = { 'Authorization': 'Bearer {}'.format(access_token) }
    response = client.post("/students/", headers=headers, json={
            'name': 'Nina Hagen', 'level': 'AP'
        })
    assert response.status_code == 201

def test_change_student(client):
    response0 = client.post("/users/login", json={'email': 'dennis@dennis1.com', 'password': 'dennis1234'})
    access_token = response0.json["token"]
    headers = { 'Authorization': 'Bearer {}'.format(access_token) }
    response = client.patch("/students/2", headers=headers, json={'level': 'AP'})
    assert response.status_code == 200
    assert response.json['level'] == 'AP'

def test_register_student(client):
    response0 = client.post("/users/login", json={'email': 'dennis@dennis1.com', 'password': 'dennis1234'})
    access_token = response0.json["token"]
    headers = { 'Authorization': 'Bearer {}'.format(access_token) }
    response = client.post("/students/2/courses", headers=headers, json={'course_id': '1'})
    assert response.status_code == 201

def test_course_double_register(client):
    response0 = client.post("/users/login", json={'email': 'dennis@dennis1.com', 'password': 'dennis1234'})
    access_token = response0.json["token"]
    headers = { 'Authorization': 'Bearer {}'.format(access_token) }
    with pytest.raises(Exception):  
        client.post("/students/5/courses", headers=headers, json={'course_id': '3'})    

def test_get_student_courses(client):
    response0 = client.post("/users/login", json={'email': 'dennis@dennis1.com', 'password': 'dennis1234'})
    access_token = response0.json["token"]
    headers = { 'Authorization': 'Bearer {}'.format(access_token) }
    response = client.get("/students/5/courses", headers=headers)
    assert response.status_code == 200
    assert response.json[0]['title'] == 'M347 Kubernetes'
